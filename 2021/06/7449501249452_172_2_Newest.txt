Alyssa Gonzalez 
1863 E. WESTMORELAND ST. PHILADELPHIA PA
Alyssamarion@icloud.com 

CAREER PROFILE:

To obtain a position in where I can grow as individual and benefit the company fully. To actively seek to enhance my skills and abilities to support the growth of the company.

EDUCATION:
2011 Graduate of Butler High School
Current Special Certifications: CPR trained / certified experience

PROFESSIONAL EXPERIENCES:

03/2017- 11-2019 Atrium Health - Wayne, NJ
Medical Receptionist - Worked at front of facility. Set/confirm appointments. Schedule/confirm interviews. Schedule transports for patients. Sign guests/family members in/out. Make schedules 2 weeks in advance. Answer phones/ redirect calls to appropriate persons. Give out new hire handbooks and proper paperwork. Filing and clerical duties as needed.  Light cleaning. 

07/2013- 02/2016 Sodexo -Northampton Community College , Bethlehem PA
Prep-Cook - Prepared food for the day as well as the following day, maintained the cooking environment, made sure everything was clean and safe for all staff members. Restocking as needed. Light grilling, frying & sandwich making. 


05/2012- 07/2013 CoWorx , Bethlehem, PA

AT&T Customer Service Supervisor -
Worked on the premier resolutions team. Responded to customer inquiries that the customer service representative was unable to resolve. Resolved customer issues and concerns to meet their needs and resolve issues. Offered solutions to customer inquiries.


2010-2012 Twinkling Starts Child Care Center, Bloomingdale, NJ
Child Care Worker 

Planned and implemented activities to meet the physical, emotional, intellectual and social needs of the children in the center. Ensured equipment and the facility were cleaned, well maintained and safe at all times.  Developed the appropriate programs and activities in collaboration with the Director. Established policies and procedures including acceptable disciplinary policies.  Remained current on all emergency and safety procedures.


REFERENCES AND FURTHER DATA WILL BE FURNISHED UPON REQUEST

