R

K

Summary

REBECCA KLEEMAN
rlkleeman97@gmail.com | 815-210-2591 | Godley, IL 60407

Personable Human Resources Coordinator versed in organizing all pre-employment training for
incoming candidates, as well as performing extensive background checks.

Skills

Experience

Microsoft Office

New employee orientations

Recruitment policies

Bills of Lading

Staff training and development

Team building

Record-keeping skills

Data Entry

Human Resources Coordinator

06/2018 - Current

Ikea Logistics | Joliet, IL
Challenged and refined current recruitment and sourcing processes by suggesting actionable
improvements and innovations.
Conducted telephone and onsite exit interviews for all employees.
Worked with HR management to devise and update policies based on changing industry and
social trends.
Helped management improve appraisal, counseling and performance optimization techniques
to maximize workforce satisfaction and productivity.
Answered employee questions during entrance and exit interview processes.
Conducted new hire orientations making new employees feel welcome and ready to succeed.
Ran queries and reports through ADP system.
Safety and Security

06/2016 - 11/2018

Ikea Logistics | Joliet, IL
Oversaw highly effective security team and managed scheduling and resource allocation.
Created and implemented programs, policies and procedures designed to effectively reduce or
eliminate injuries and hazards in workplace.
Guided store trainings on safety initiatives, active shooter and emergency management
procedures.
Conducted regular inspections of facilities to check compliance with fire, hazard and safety
guidelines.
Examined hazardous incidents and accidents to uncover the causes and provided corrective
actions.
Analyzed health and safety data to verify safety program performance, identify areas of
concern and recommend improvement plans.
Logistics Coordinator

01/2015 - 06/2016

Ikea Logistics | Joliet, IL
Loaded, unloaded, moved and sorted materials to keep items flowing to correct locations.
Checked items into computer system, printed labels and directed to storage locations.
Obtained and coordinated materials required to meet contract objectives.
Improved operations by working with team members and customers to find workable solutions.

�Education and
Training

Some College (No Degree): Business Administration And Management
Joliet Junior College | Joliet, IL

�

-----END OF RESUME-----

Resume Title: HR - Recruiting

Name: Rebecca Kleeman
Email: rlkleeman97@gmail.com
Phone: true
Location: Godley-IL-United States-60407

Work History

Job Title: Human Resources Coordinator 06/01/2018 - Present
Company Name: Ikea Logistics

Job Title: true 06/01/2016 - 11/30/2018
Company Name: Ikea Logistics

Job Title: Logistics Coordinator 01/01/2015 - 06/01/2016
Company Name: Ikea Logistics

Education

Additional Skills And Qualifications

Most Recent Job Title: true
Most Recent Pay: USD per
Managed Others: true
Languages Spoken: English
Work Authorization: true
Security Clearance: No
Felony Conviction: No

Desired Pay: USD per
Desired Job Types: Full Time
Travel Preference:

Currently Employed: No
Jobs Last Three Years: 0
Last Job Tenure Months: 0
Highest Degree: true
Certifications:
Employment Type:
Military Experience:
Last Updated: 5/5/2021 12:00:00 AM

Desired Shift Preferences:

Interests:
General Business
Supply Chain
Business Development
Consultant
Strategy - Planning
Telecommunications
BPO-KPO

Downloaded Resume from CareerBuilder
RD61NB6WRQDGJQ7MF8N


