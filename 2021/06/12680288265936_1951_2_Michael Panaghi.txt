Michael
Panaghi

Mpanaghi@me.com
516-852-6792
Winter Garden, FL 34787

Summary
A creative education specialist who motivates and engages learning audiences and clients through dynamic presentation and encouraging validation. Positive influencer for driving sales and supporting company ROI, leadership team development, and individual associate growth for achieving maximum results and revenue.

Skills

Solid Leadership Development
Increasing Revenue & ROI
Motivating Training Focus
Collaborative Partnering
Artistic Makeup Design
Strong Client Focus
Skincare Ingredients Knowledge
Felid stagey implement

Experience
PRODUCT EDUCATION MANAGER-COLOR | 01/2021 to Current
The Avon Company
Designed and directed orientation and training programs to meet Representative' needs.
Organized and prepared instructional materials, communique, and reports to facilitate learning.
Educated Representatives on the benefits of products with a full consultation, including cosmetic application.
Executed biweekly Live virtual training showcasing new products and applications, coaching on best practices, and selling skills.
Hosted New Products during sales conference for Representatives with over 7,000 Representatives attending.
Developed complete training programs and led training using expert learning techniques.
Led training classes and recorded instruction for later use.
Building PowerPoint presentation to help drive home training.
Partnered with Marketing, Legal, and R&D for approvals on all works of training.

Sales Executive | 05/2020 to Current
New Avon
Managed a $2.9 million market.
New employee onboarding, peer mentoring with active coaching support to sales.
Exceeded sales goals, improved profitability by aligning sales strategies with business plans within market trends, which led to 101.2% Q3 Sales.
Achieved proven results through practical sales goals, creative sales campaigns, Representatives/Store development, and teamwork.
Executed weekly virtual training, developments with Avon Representatives on product developments, product ingredients, with best practices to sell products.
Executed weekly in-store person training with Sephora on product ingredients, best practices, ensure Belif Sales in every Sephora purchase while ensuring the mission statement of Belif upholds.
Stayed current on industry trends/changes while participating in professional development opportunities to strengthen product and service knowledge.
Delivered performance updates, quarterly business reviews, including planning meetings.
Attended all company sales training to improve skills and learn new selling techniques.
Implemented strategies to drive changes and improvements while enhancing performance.
Met with Regional Vice President to assure proper scheduling of projects and update project timelines.
Account Executive | 08/2015 to 03/2020
First Aid Beauty
Manage a $1.2 Million territory with a Year-to-Date increase of +22% throughout the East Coast of the United States and Puerto Rico.
New employee onboarding, peer mentoring with active coaching support to sales.
Sephora Business in Florida and Puerto Rico at a +10% Year-to-Date.
Ulta Beauty Business in Florida and Puerto Rico is at +67.3%.
They were planned while executed high-volume sales events within five different markets, including exceeded goals.
Recruit, train new talent to drive sales in the market.
Coach and develop the freelance team for them to achieve personal and professional goals.
Conduct Training in store, including for Sephora.com.
I exceeded Last Year Plan by 13%.
Conducted and Executed Grand Store Openings for Sephora and Ulta.
Help train new Account Executives from other markets.
Develop relationships with management across all channels to help invigorate and maintain a favorable business.
Ensure store planogram and attractive for consumers to shop.
Participated in lab studies to help develop new products.
Social media expert contributing to follower expansion of FAB Twitter and Facebook accounts.
Worked closely with the leadership team to develop and implement effective marketing strategies.
Implemented improved methods to assist with marketing campaign performance.
Edge Business Manager/ and South Florida Regional Trainer | 01/2012 to 07/2015
Dillard's - Orlando, Florida
Manage 31 different lines inside the Edge shop with 1 million in total sales.
Achieved a $25,000 increase in total sales volume since I took over.
Achieved the most significant increase in sales for the Southern Woman's Show from $12,600 last year to $24,000 this year.
Achieved #1 in Region.
Develop and execute yearly sales plans for each store.
Instruct Area Sales Managers on business analysis and KPI's.
Facilitate training seminars on organizational standards, product knowledge, selling techniques, customer service, and team collaboration.
Network with local medical professionals and breast cancer support groups to develop partnerships and enhance their patient's care before and after surgery through Dillard's Beauty Services.
Regional Makeup Artist | 12/2009 to 01/2012
Elizabeth Arden
Manage Central and South Florida artists.
Manage 25 Macy's accounts.
Coached onboarding while supporting regional events.
Coordinating corporate events with all 25 accounts semi/annual events in conjunction with Gift with Purchase.
Communicating with counter managers/department managers to assure appointments by booking while ensuring client turn out.
Executing presale to ensure complete booking for the event to ensure profitable Events.

Education and Training
Eastport High School - Eastport, NY
High School Diploma
The University of South Florida - Tampa, FL
Some College (No Degree)
Post- Crisis Leadership

Activities and Honors
Account Executive of the year 2017

.

.

-----END OF RESUME-----

Resume Title: Sales Executive

Name: Michael Panaghi
Email: mpanaghi@me.com
Phone: (516) 852-6792
Location: Miami-FL-United States-33122

Work History

Job Title: true 01/01/2021 - Present
Company Name: PRODUCT EDUCATION MANAGER-COLOR; The Avon Company

Job Title: Sales Executive 05/01/2020 - Present
Company Name: New Avon

Job Title: Account Executive 08/01/2015 - 03/31/2020
Company Name: First Aid Beauty

Job Title: Edge Business Manager/ and South Florida Regional Trainer 01/01/2012 - 07/31/2015
Company Name: Dillard's

Job Title: Regional Makeup Artist 12/01/2009 - 01/01/2012
Company Name: Elizabeth Arden

Education

School: Eastport High School - Eastport, NY, Major: Not Applicable
Degree: High School, Graduation Date:

School: University of South Florida - Tampa, FL Some College, Major:
Degree: None, Graduation Date:

Additional Skills And Qualifications

Most Recent Job Title: true
Most Recent Pay: USD per
Managed Others: true
Languages Spoken:
Work Authorization: Not Specified
Security Clearance: No
Felony Conviction: No

Desired Pay: USD per
Desired Job Types: ["Full Time","Part Time","Intern","Seasonal","Temporary","Contractor"]
Travel Preference:

Currently Employed: No
Jobs Last Three Years: 0
Last Job Tenure Months: 0
Highest Degree: None
Certifications:
Employment Type:
Military Experience:
Last Updated: 4/5/2021 3:39:53 PM

Desired Shift Preferences:

Interests:
Sales
Business Opportunity
Franchise
Internal Sales

Downloaded Resume from CareerBuilder
RD781M5ZCHQ7KYW655W


