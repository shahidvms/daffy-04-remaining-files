123 Oakbrook Circle
Pittsburgh, PA 15220-4655

Phone
Home:412-922-8684
Cell: 412 370 7933
E-mail mzrust123@comcast.net
Marilyn E. Zrust
Name:Marilyn Zrust, EdD, MSN, RN
Birth Place:United States of
America
Citizenship: American

Education and Training
Undergraduate
1979University of PittsburghBSNNursing
School of Nursing
Pittsburgh, PA

1980Community College of CertificateEMT
Allegheny County
Pittsburgh, PA

1980-1981Community College of Certificate Paramedic
Allegheny County,
West Mifflin, PA

1983University of PittsburghStatistics
College of Arts & Sciences
Pittsburgh, PA

Graduate
1989University of PittsburghMSN
School of Nursing Nursing
Pittsburgh, PA

Postgraduate
2013University of PittsburghEdD
School of EducationHigher Education Pittsburgh, PAManagement
Appointments and Positions
Non-Academic
2012, DecemberSTAT StaffingStaff Nurse Relief:
To present40 24TH StreetUniversity health offices
Pittsburgh, PA 15222 Substitute school nurse;
(412) 434-7828 various districts, public & private
Children s Institute
Children s Home
Health South Rehab
Vincentian Manor
Verland Home

2006, summer West Penn Allegheny Health SystemStaff Nurse,
The Western Pennsylvania HospitalEmergency Department
Pittsburgh, PACasual Pool

1980-1989St. Francis Central HospitalFull Time Staff Nurse ER
(formerly Central Medical)
Pittsburgh, PA
1990-1994Off Shift Clinical
Supervisor

1984-1986St. Francis Central Hospitalprn pool:
Pittsburgh, PAICU, IV Team
Ambulatory Care Center

1979-1980Allegheny General HospitalStaff Nurse
Pittsburgh, PATrauma-
Neuro Unit

1980.1981 Baldwin Emergency Medical Services
Emergency Medical Technician and Paramedic, Volunteer

Academic
2018Slippery Rock University
Spring, FallTemporary/Adjunct
Department of Nursing
Slippery Rock, PA

This faculty position was a half time assignment teaching in an on-line RN-BSN program. Courses taught were Nursing Care of the Elderly and Health Assessment.

2016-2017Wheeling Jesuit UniversityAssociate
Department of NursingProfessor
Wheeling, WV

This faculty position included the didactic, clinical & clinical lab. Classes taught include: Foundations of Nursing, Pediatric Nursing, Senior Seminar, Introduction to Pathophysiology.

2014-2016West Virginia Wesleyan CollegeAssociate
School of Nursing Professor
Buckhannon, WV

2013- 2014Alderson Broaddus UniversityAssociate Professor,
School of NursingChair, School of Nursing,
Philippi, WVDirector, On- Line RN-BSN
Program

2005-2006University of PittsburghClinical Instructor, Skills
Part-time; adjunctSchool of Nursing and Simulation Lab
2006-2011Acute/Tertiary Care DepartmentInstructor, Guest lecturer
Full timePittsburgh, PA

2002- 2005Duquesne UniversityClinical &
Part time; adjunctSchool of Nursing Skills Lab
Pittsburgh, PAInstructor

2002-2006West Penn Allegheny Health SystemInstructor:
Part time; adjunct Western Pennsylvania School of Nursing Clinical &
Pittsburgh, PATheory

2000-2001Ohio Valley General HospitalInstructor:
Part time; adjunct School of NursingClinical &
McKee s Rocks, PATheory
1997-1999Parkway West Area Vocational/Practical Nurse
Part time; adjunct Technical Institute Instructor: Clinical &
Oakdale, PATheory

1989-1997Community College of AlleghenyClinical Instructor,
Part time; adjunctCounty, South Campus
West Mifflin, PA

LICENSURE AND CERTIFICATION

1979-presentRN-230908-LPA

2013-present86342WV

1980.1982 Emergency Medical PA
Technician

1981-1984EMT- Paramedic IPA
Pennsylvania37862

1979-presentBasic Life Support Provider

1980, 1984Instructor of Basic Life
Support

1982.1985 Advanced Cardiac Life Support Provider
2000-2008

1982.1985 Instructor of
Advanced Cardiac Life Support

11/2001-2007Pediatric Advanced Life Support
(PALS)

5/2001-2007Neonatal Resuscitation

Membership in Professional Societies
American Nurses Association (ANA) 2010-present

Pennsylvania State Nurse Association (PSNA)-2010-present

Member of the Political Action Committee (PAC) as of 2011-2018
West Coordinator for the Government Relations Committee 2011-2018

District 6 PSNA 2010-2017 (closure of the district)
Immediate Past-President, January 2015-2017
President for a two-year term, beginning January 2013 to 2015.
President elect of the PSNA for a two-year term January 2011-2013,

Membership in Honors Societies
April 26, 2008-presentPi Lambda Theta Education Honor Society
April 15, 2015-presentSigma Theta Tau, Honor Society of Nursing
Epsilon Delta at Large Chapter

Unpublished Presentations

Zrust, Marilyn. (1983-1989) St. Francis Central Hospital: In Service Educational Programs: Preservation of Criminal Evidence in the Emergency Department, Basic EKG Interpretation, Prevention of Decubitus Ulcers, Patient Education, Review and Use of IV Equipment.
Special Assignment
Preceptor for Kristen Sandridge for her teaching practicum as partial fulfillment of her MSN in nursing education at the University of Pittsburgh during Fall 2010 and Spring 2011

Publications
Zrust, M. (2013). Human Capital Assessment Indicators as Influential Determinants Pertaining to the Admission Criteria Utilized by Pre-licensure Programs for Nursing Education. University of Pittsburgh.
Zrust, M. (2012). Legislative Day: A Chance for Pennsylvania Nurses to Be Seen, Heard and Supported. Vital Signs, 64 (17), p. 3.

Scholarly Presentations
Crisis Response: The Patient or Visitor with Potential for Violence . Presented at District 6 Pennsylvania State Nurses Association, May 10, 2013
Spirituality in Nursing Care: Presented at District 6 Pennsylvania State Nurses Association, November 13, 2012

Human Capital Assessment Indicators as Influential Determinants for the Admission Criteria for Initial Education Programs for Nursing Education. Presented at: Council of Graduate Students in Education 1st Annual Research Colloquium, November 3, 2011. University of Pittsburgh, School of Education.

Presented summary of the meeting of the PA Center for Women and Politics, as well as presenting the key points from the PSNA White Paper. PSNA District 6, September 27, 2012

Service
PSNA (Pennsylvania State Nurses Association)
September 26, 2011Attendance at the Summit on Advancing Nursing
Education in Pennsylvania.

May 8, 2012Attendance at Legislative Day, Harrisburg, PA

May 31, 2012Town Hall Meeting Sponsored by: PA State Nurses Association, Excela Health and PSNA District 6 at:
The University of Pittsburgh at Greensburg

April 15, 2013Attendance at Legislative Day, Harrisburg, PA

March 20, 2014Town Hall Meeting Sponsored by: PA State Nurses Association, PSNA District 6 and the University of Pittsburgh, School of Nursing

March 31, 2014Attendance at Legislative Day, Harrisburg, PA

April 30, 2015Attendance at Legislative Day, Harrisburg, PA

April 11, 2016Attendance at Legislative Day, Harrisburg, PA

Sigma Theta Tau, Epsilon Delta at Large Chapter

June 1, 2016-May 31, 2018Secretary

WVWC School of Nursing

September, 2015 Represented the WVWC School of Nursing in a public service information piece on the dangers of caffeine, for WBOY TV station The video link is no longer available, but the text of the interview is. Please be advised that there are numerous transcription errors. (https://www.wboy.com/archives/officials-warn-of-drug-use-to-support-studying/Sensitive information was removed).

Alliance of Nurses for a Healthy Environment (AHNE)

January, 2017Participated in a press conference regarding the effects of methane pollution, sponsored by the Alliance of Nurses for a Healthy Environment (AHNE). I spoke about the health effects of methane pollution from a nurse s perspective. This event was covered by KDKA Radio, Pittsburgh, and there was a radio clip of my speech

-----END OF RESUME-----

Resume Title: Healthcare - Nursing - RN

Name: Marilyn Zrust
Email: mzrust123@comcast.net
Phone: true
Location: Pittsburgh-PA-United States-15220

Work History

Job Title: Staff Nurse Relief 12/01/2012 - Present
Company Name: STAT Staffing; 40 24TH Street University health offices; Health South Rehab

Job Title: Staff Nurse 01/01/2006 - 03/15/2021
Company Name: West Penn Allegheny Health System; The Western Pennsylvania Hospital

Job Title: Emergency Medical Technician and Paramedic, Volunteer 01/01/1900 - 03/15/2021
Company Name: true

Job Title: Supervisor 01/01/1990 - 12/31/1994
Company Name: true

Job Title: Staff Nurse ER (formerly Central Medical) 01/01/1980 - 12/31/1989
Company Name: St. Francis Central Hospital

Job Title: ICU, IV Team 01/01/1984 - 12/31/1986
Company Name: St. Francis Central Hospital

Job Title: Staff Nurse 01/01/1979 - 12/31/1980
Company Name: Allegheny General Hospital

Education

School: University of Pittsburgh; School of Nursing Pittsburgh, PA, Major:
Degree: Bachelor's Degree, Graduation Date:

School: Allegheny County Pittsburgh, PA, Major:
Degree: None, Graduation Date:

School: Community College of; Allegheny County, Major:
Degree: None, Graduation Date:

Additional Skills And Qualifications

Most Recent Job Title: true
Most Recent Pay: USD per
Managed Others: true
Languages Spoken: English
Work Authorization: Not Specified
Security Clearance: No
Felony Conviction: No

Desired Pay: USD per
Desired Job Types: Full Time
Travel Preference:

Currently Employed: No
Jobs Last Three Years: 0
Last Job Tenure Months: 0
Highest Degree: None
Certifications:
Employment Type:
Military Experience:
Last Updated: 3/15/2021 12:00:00 AM

Desired Shift Preferences:

Interests:
Nurse

Downloaded Resume from CareerBuilder
R2Y1MP6RNP39PJDR1W9


