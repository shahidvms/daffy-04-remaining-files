
Jephte Joassaint

540 NW Fourth Ave
Fort Lauderdale, FL 33311
Cell: 9548502113

Professional Summary

Dependable, serving customer needs in Warrhouse environments. Pleasantly offers assistance in finding wanted products. Considered hardworking, punctual, and reliable.

Skills

Drawer balancing

Product restocking

Online orders preparation

Loyalty program promotion

Report preparation

Aisle cleaning

Shipping and receiving tracking

Shipping coordination

Experience

August 2016 to August 2019
McDonald's  Fort Lauderdale, FL
Cashier     

Maintained clean, organized, and well-stocked checkout areas.
Stocked store shelves and end-caps with merchandise during down times.
Balanced cash drawer at beginning and end of each shift.
Provided information to customers on products or services.
Explained promotions and special offers to customers.

August 2019 to August 2020
Men's Wearhouse 
Sales Associate Manager    

Trained sales team on superior strategies for managing value messages, structuring opportunities, and achieving deal closures.
Analyzed sales performance, coverage, and segmentation and distributed KPI reports to illustrate findings.
Developed rapport with clients and key prospects, including actively contributing to sales process, making in-person sales calls, and entertaining clients.

August 2020 to Current
Boars head Fort Lauderdale, FL
Shipping and Receiving Associate    

Reconciled shipping invoices and receiving reports to verify count accuracy.
Staged merchandise by department, marked appropriately for placement in facility, and delivered merchandise to different departments.
Maintained accurate tracking system covering shipping and receiving transactions.
Pulled and prepared products for shipments by operating cherry pickers or reach trucks.
Protected staff and general public by following hazardous material shipping guidelines and regulations.

Education

Fort Lauderdale High School  

