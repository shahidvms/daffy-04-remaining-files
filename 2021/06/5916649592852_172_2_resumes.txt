Adrienne Blackwell
27051 Brush ave apt 85
Euclid Ohio 44132
- 2162097191 (H) - 2162097191 (C) - mzprettyeyez76@gmail.com
 
 Professional Summary
Dedicated Customer Service Representative motivated to maintain customer satisfaction and 
contribute to company success.Customer-focused Representative with a proven capacity to 
troubleshoot issues to ensure customer satisfaction.Customer Service Representative who 
maintains a high level of professionalism, patience and efficiency to minimize customer 
dissatisfaction and increase customer loyalty.Seasoned customer service specialist with 
background in providing advice on diverse customer situations.High-powered Customer Service 
Manager who effectively motivates associates through continual guidance, direction, 
development and coaching.Self-motivated banking professional offering strong communication 
and quick thinking skills. Works effectively on teams, as well as independently, in fast-paced 
environments.Enthusiastic Customer Service Representative who follows policies and procedures, 
while offering a friendly customer environment.
Skill Highlights
Exceptional customer service                                       Sharp problem solver
 MS Office proficient                                                          Courteous demeanor
 Strong organizational skills                                          Energetic work attitude
 Active listening skills                                                        High customer service standards
 Seasoned in conflict resolution
 Employee relations specialist
 Work Experience
The Laurels Of Chagrin
April 2018 to Current
STNA
CHAGRIN FALLS OHIO
Observed and documented patient status and reported patient complaints to the case 
manager.Read and recorded temperature, pulse and respiration.Massaged patients and applied 
preparations and treatments, such as liniment, alcohol rubs and heat-lamp 
stimulation.Completed and submitted clinical documentation in accordance with agency 
guidelines.Helped physicians examine and treat patients by assisting with instruments, 
injections and suture removal. And do schedule for different shifts 
 omni Care
August 2009 to Current
field person/CNA
Beachwood, OH
Observed and documented patient status and reported patient complaints to the case 
manager.Read and recorded temperature, pulse and respiration.Massaged patients and applied 
preparations and treatments, such as liniment, alcohol rubs and heat-lamp 
stimulation.Completed and submitted clinical documentation in accordance with agency 
guidelines.Helped physicians examine and treat patients by assisting with instruments, 
injections and suture removal.
Willow Park
August 2005 to March 2009
Medical Records/Front desk
Warrensville Heights, OH
Reviewed clients' accounts and results regularly to determine whether life changes, economic 
developments or financial performance indicated a need for plan revision.Evaluated patient care 
procedural changes for effectiveness.
Rain Soft
January 1997 to April 1999
collections
Warrensville Heights, OH
Collected customer feedback and made process changes to exceed customer satisfaction goals.Made 
reasonable procedure exceptions to accommodate unusual customer requests.Provided accurate 
and appropriate information in response to customer inquiries.Demonstrated mastery of customer 
service call script within specified timeframes.Addressed customer service inquiries in a timely 
and accurate fashion.Maintained up-to-date records at all times.Built customer loyalty by placing 
follow-up calls for customers who reported product issues.
Education and Training
Remington
2007
Associate of Arts: coder
Maple Heights, OH
 Coursework in [][<4.0] GPA

 
Shaker Heights High
 
High School Diploma
Shaker heights, OH, cuyhoga
[3.9] GPA

 
 1994
