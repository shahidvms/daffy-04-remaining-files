Abel Michel JR.
424 chestnut st
New Britain, CT 06051
646-404-3094
Abelmichel57@yahoo.com 

OBJECTIVE: To obtain a position that will utilize my skills.                                       

EXPERIENCE:

Security U.S.A 
336 W 37th st #450
New York, NY 10011
7/2016 -- 2/2020

Front Desk Security for residential site:
Monitor the cameras, sign in for packages, stay alert of intruders, sign in and use telecom to notify residents of any guests or food delivery, notify the Super of any situation, greet all residents as they exit or enter the building.

Esteem Security
133 W 19th Street #6
New York NY, 10011
4/2012 – 1/2015

I-park Security: control the fire board, control the fireworks computer, inform engineers of any fire alarm, inform any brownouts, turn smoke devices on or off, turn water flows and tampers on or off,
reset fire board when needed, make announcements of any test alarms, watch cameras and parking lots, and inform L.I.J hospital when I’m turning smoke devices off or on in their section.

SilverCrest Nursing Home
144-45 87th Ave
Jamaica NY, 11435 

4/2007 - 1/2011

 Dietary aide: set-up food trays, serve hot food, deliver food carts, wash pots and pans, make coffee, sweep, mop, wrap silverware, and work the dish machine.

 EDUCATION:
H.S DIPLOMA
John Adams H.S
101-01 Rockaway BLVD 
Ozone Park NY, 11417

 QUALIFICATIONS: 8hr, 16hr, fireguard license, security guard license, bilingual, and computer literate.

REFERENCES: FURNISHED UPON REQUEST   