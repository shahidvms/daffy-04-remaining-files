
Marilyn Gogue

10050 Twin Lakes Drive
Coral Springs, Fl 33071
Cell: 754-235-8260
E-Mail: Marilyngogue75@gmail.com

Professional Summary

Creative and forward-thinking young three's teacher dedicated to student success. Focused on continuous improvement of curriculum and classroom management strategies. Coach and motivate teachers while working effectively with administrators and parents to affect important changes.




Dedicated administrative assistant industry professional with demonstrated strengths in customer service, time management and trend tracking. Good at troubleshooting Customer service problems and building successful solutions. Excellent verbal and written communicator with strong background cultivating positive relationships and exceeding goals.

Skills

PC, MS Office (Word Excel

Performance driven administrative abilities in facilitating support services and office management

Strong telephone and personal communication skills

Dean's list Fall 2012 Valencia College

Dean's list Spring 2013 Valencia College

Energetic and courteous team player

Proficient in meeting deadlines and meeting highly essential schedules, DCF Child Care

Certificate in Office Support & Medical Office Support.

Experience

January 2017 to January 2018
Children's World of Margate Margate, FL
Lead Teacher    

Young three's.
Created lesson plans, crafts, and matching activities.
Engaged children in learning play.

January 2015 to January 2017
Served Lunch and Snack, VPK Academy Poinciana, FL
Lead Teacher    

3/4 year old class.
Created lesson plans, crafts and matching activities.
Engaged children in learning play.

March 2014 to April 2014
B & T Medical Center Kissimmee, FL
Front desk    

Clerical.
Answering calls.
Signing in patients.
Scheduling patient appointments.

August 2013 to October 2013
Celebration Health Assessment, Florida Hospital Celebration, FL
Marketing team member    

Scheduling appointments(answering phones).
Emailing campaign.

December 2008 to February 2009
General Insulation Pompano Beach, FL

Answering phones.
Light bookkeeping.
Light collection calls.
Clerical.

December 2000 to June 2007
Kenco Logistic Services Pompano Beach, FL
Traffic Coordinator    

Managed multi-line phone systems involving a large amount of calls per day.
Provided assistance to customers in locating customer products and orders in an effort to maintain total customer satisfaction enhancing loyalty between the customer and the company.
Facilitated the resolution of any discrepancies involving documentation or issues that the customer needed resolved.
Created and reviewed company procedures in order to comply with customer requirements and internal departments.
Created opportunities cross train in several positions with office personnel to maintain constant fluidity, success, and improvement of office structure.
Clerical – Responsible for filing daily driver manifests and proof of deliveries along with vehicle inspection reports.
Kept daily reports of driver information on spreadsheet formats making certain all information was correct and sent to the appropriate department.
Improved procedures in maintaining driver personnel records and DOT information.
Improved filing system of paperwork with customer signatures for future reference.
Improved method of record keeping in regards to codes used for billing purposes.
Dispatcher – Handled communications between drivers, customers, and office.
Uploaded and downloaded information customer signature for proof of deliveries.
Set up routes for drivers for the following day.
Responsible for DOT information all drivers.
Improved the proof of delivery program from an older version to a newer version.
Improved procedures between customer, driver, and office personnel.
Set up a more efficient procedure for information management.

Education

January 2013 Florida Dept. of Children and Families, Valencia College Kissimmee, Fl
Technical Certificate (Medical Coder/Builder)  

January 1999 Florida Atlantic University Boca Raton, FL
Bachelor of Arts  

January 1996 Broward Community College Coconut Creek, FL
Associates Degree  

References

References available upon request

Languages

Fluent in Spanish and English