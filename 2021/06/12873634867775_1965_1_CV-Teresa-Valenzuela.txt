 
 
Teresita Valenzuela
My objective is to obtain a challenging leadership position where I can improve my work skills and gain further experience while enhancing the company’s productivity.
EXPERIENCE
Cats USA Pest Control Inc.
North Hollywood, CA — Marketing Receptionist
NOVEMBER 2020 - FEBRUARY 2021
● Answering high volume of calls
● Giving quotes to new and existing customers ● Give technicians their daily routes
US HealthWorks
Gardena, CA — Medical Assistant
AUGUST 2011 - APRIL 2019
● Scheduling/Arranging appointments for patients
● Handled front and back office for verification of workers comp ● Performed drug and vital sign screening for patients
Venice Culver Marina Medical Group Culver City, CA — Medical Assistant
FEBRUARY 2002 - JULY 2011
● Billing
● Handled workers comp
● Performed EKG, vital signs, and drug screenings ● Scanned holter monitor
Martin Luther King Jr. Hospital - Cardiology Dept. Los Angeles, CA — Cardiac Technician
NOVEMBER 1995 - JANUARY 2002
● Assist doctor with stress test ● Scanned holter monitor ● Performed EKG
EDUCATION
American College Career, Gardena, CA — Medical Assistant DECEMBER 1998
Westchester High School, Los Angeles, CA — High School Diploma AUGUST 1991 - JULY 1993
1032 S. Osage Ave Apt. 5 Inglewood, CA 90301
(310) 879-7536 tcv37hot@hotmail.com
SKILLS Phlebotomy Cardiac Technician Holter Monitor Screening Patient Scheduling Insurance verification
Specimen Collections and Analysis
Clerical
Verifying Authorization for Workers Comp.
Attention To Detail
LANGUAGES
Bilingual- Fluent English and Spanish
REFERENCES
Available upon request.