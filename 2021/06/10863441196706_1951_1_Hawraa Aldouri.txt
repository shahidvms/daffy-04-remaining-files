Hawraa Aldouri
CONTACT
Simsbury, CT
860.818.4999
hawraa.aldouri2016@gmail.com

EDUCATION

Bachelor of Science
Biomedical Engineering
University of Hartford
2014 - 2018
Capstone Topic
Smart Hard Hat
Masters of Science
Management
University of Hartford
2018 - 2020

SKILLS

EXPERIENCE
07/2017 - 07/2018Saint Francis Hospital
Hartford, CT
Biomedical Engineer Intern
Maintaining and fixing medical equipment and
devices.
Monthly inventory - Mainly for IV pumps.
10/2017 - 02/2018CVS Pharmacy
Granby, CT
Pharmacy Technician
Preparing prescriptions.
Delivering medications to patients.
Providing help and assistance for customers.
10/2015 - 10/2016Swarovski
West Hartford, CT
Sales Associate
Introducing products to customers.
Building a trust relationship with customers.
Creating selling plans.
Seasonal Inventory.

PROJECTS

COMSOL
Matlab
Microsoft Office
AutoCad
Labview
Kinovea

Fall 2017

REFERENCES

AWARDS

Alireza Jamalipour
Professor, University of Hartford
860.594.3221
Asaki Takafumi
Professor, University of Hartford
860.888.5766

2017- 2018

May 2018

Fall, 2015 - 2017

Spring, 2018

Biomechanics: Gait Data Analysis; Knee and Hip
joints Kinetics in Relation to Squat Loads
EMG Lab projects.
Biomechanics to Collect and Analyze Data &
Standing Balance .
Senior Design Project: Smart Hard Hat ,
BiofluidMechanics:LaminarFlowinCardiovascular
Bioinstrumentation: IV Pumps

American Society for Engineering Education
First Place Award for Senior Design Project
(Smart Hard Hat)
Dean s List
President s Honors List

�

-----END OF RESUME-----

Resume Title: Engineering - Biomedical

Name: Hawraa Aldouri
Email: hawraa.aldouri2016@gmail.com
Phone: true
Location: Simsbury-CT-United States-06070

Work History

Job Title: Biomedical Engineer Intern 07/01/2017 - 07/31/2018
Company Name: Francis Hospital

Job Title: Pharmacy Technician 10/01/2017 - 02/28/2018
Company Name: Pharmacy

Job Title: Sales Associate 10/01/2015 - 10/31/2016
Company Name: true

Education

School: University of Hartford, Major:
Degree: Bachelor's Degree, Graduation Date:

School: University of Hartford, Major:
Degree: Master's Degree, Graduation Date:

Additional Skills And Qualifications

Most Recent Job Title: true
Most Recent Pay: USD per
Managed Others: true
Languages Spoken: English
Work Authorization: Not Specified
Security Clearance: No
Felony Conviction: No

Desired Pay: USD per
Desired Job Types: Full Time
Travel Preference:

Currently Employed: No
Jobs Last Three Years: 0
Last Job Tenure Months: 0
Highest Degree: Graduate Degree
Certifications:
Employment Type:
Military Experience:
Last Updated: 4/23/2021 12:00:00 AM

Desired Shift Preferences:

Interests:
Biotech

Downloaded Resume from CareerBuilder
RD88GH6XWR83P0ZSZRR


