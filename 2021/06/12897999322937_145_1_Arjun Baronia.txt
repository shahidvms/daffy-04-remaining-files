EDUCATION

Washington University in St. LouisSep 2017-May 2021
Major: Biology
Graduation Date: May 2021

RESEARCH EXPERIENCE

Washington University in St. Louis School of Medicine; Research Assistant; St. Louis, MO. Sep 2018-Dec 2019
Worked in the department of pulmonary medicine under Dr. Jennifer Alexander-Brett, analyzing the role of small extracellular vesicle exosomes in chronic lung disease
Separated exosomes from free proteins in bronchial wash fluid through size-exclusion chromatography and analyzed them through Western blotting the column fractions
Conducted immunostaining on COPD cells to better understand the exosome trafficking pathway in chronic lung disease
Created a variety of stock solutions for lab use
Awarded BioSURF research fellowship for Summer 2019

University of Massachusetts Medical School; Research Assistant; Worcester, MA. Jun 2016-Aug 2016
Examined the effects of copper ion staining on myoblast cells under Dr. Teresita Padilla-Benavides
Examined gene cloning, miniprep for DNA purification, mammalian cell culture, and protein electrophoresis

University of Massachusetts Medical School; Research Assistant; Worcester, MA. Jun 2015-Aug 2015
Examined the localization of Evi and Wg genes in the neuromuscular junction of Drosophila Melanogaster both presynaptically and postsynaptically through confocal microscopy under Dr. Catalina Ruiz-Canada

VOLUNTEER EXPERIENCE

Crisis Text Line; Crisis CounselorJun 2020-Present
Helped counsel 100+ texters dealing with domestic abuse, eating disorders, self-harm, relationship issues, and mental health disorders
Completed 30 hour crisis training in suicide risk assessment, mandatory reporting, and crisis management

Learning Lodge; Academic TutorMay 2020-Present
Provided 100+ hours of reading, writing, science, and social studies instruction to a first-grade student
Created new lesson plans each week targeting different areas of improvement for the student

Campus Kitchen; CookJan 2020-Mar 2020
Helped prepare meals for those in underprivileged communities on a weekly basis

PROJECTS

Phone a Grandparent; Co-founderMay 2021-Present
Started a 501(c)(3) non-profit organization with the goals of connecting older adults with college and high-school age volunteers with weekly phone calls to alleviate senior loneliness during the COVID-19 pandemic
Coordinated with senior centers all over the nation to bring a variety of older adults and volunteers together

SKILLS AND ABILITIES

Western blots, Immuostaining, Chromatography, Counseling, Writing and Editing, Academic Tutoring, Communicating

PUBLICATIONS

Epithelial IL-33 appropriates exosome trafficking for secretion in chronic airway disease, JCI Insight Feb 2021

-----END OF RESUME-----

Resume Title: Healthcare - Allied Health - Behavioral

Name: Arjun Baronia
Email: abaronia108@gmail.com
Phone: true
Location: Saint Louis-MO-United States-63199

Work History

Job Title: Crisis Counselor 06/01/2020 - Present
Company Name: Crisis Text Line

Job Title: Academic Tutor 05/01/2020 - Present
Company Name: Learning Lodge

Job Title: Cook 01/01/2020 - 03/31/2020
Company Name: Campus Kitchen

Job Title: Research Assistant 09/01/2018 - 12/31/2019
Company Name: Washington University in St. Louis School of Medicine

Job Title: Research Assistant 06/01/2016 - 08/31/2016
Company Name: University of Massachusetts Medical School

Job Title: Research Assistant 06/01/2015 - 08/31/2015
Company Name: University of Massachusetts Medical School

Education

School: Washington University, Major:
Degree: Bachelor's Degree, Graduation Date:

Additional Skills And Qualifications

Most Recent Job Title: true
Most Recent Pay: USD per
Managed Others: true
Languages Spoken: English
Work Authorization: Not Specified
Security Clearance: No
Felony Conviction: No

Desired Pay: USD per
Desired Job Types: Full Time
Travel Preference:

Currently Employed: No
Jobs Last Three Years: 0
Last Job Tenure Months: 0
Highest Degree: 4 Year Degree
Certifications:
Employment Type:
Military Experience:
Last Updated: 6/8/2021 12:00:00 AM

Desired Shift Preferences:

Interests:
Education
Training
Teaching

Downloaded Resume from CareerBuilder
RD97GG5YNQ9Z8ZQMDTP


