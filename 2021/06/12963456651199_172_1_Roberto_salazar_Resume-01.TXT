
Roberto salazar

Professional Summary

Efficient, hard-working Baker with over 10 year background in fast-paced bakery settings. Reliable and supportive team player.

Work History

June 2018 to Current
Big Apple Bagels Kalamazoo, MI
Baker/delivery   
Part time baking of orders for wholesale delivery. Once orders are baked and packaged I deliver via work vehical to wholesale locations. Some cleaning duties are required as well.
November 2004 to July 2017
Big Apple Bagels Kalamazoo, MI
Baker/delivery    
Part time baking of orders for wholesale delivery. Once orders are baked and packaged I deliver via work vehical to wholesale locations. Some cleaning duties are required as well.
        graphic design

        I also have done freelance graphic design . Using Adobe Photoshop, Illustrator and Indesign. I have worked on and made for clients brochures, product design, logos and flyers.

Kalamazoo, MI 49009
Phone: 1-269-359-2038
E-Mail: nightboatmusic2@yahoo.com

Skills

Baking, Cleaning, delivery, food prep 

Education

Kalamazoo Central Kalamazoo, MI
High School Diploma 
KVCC- 2 year associate (graphic design)
