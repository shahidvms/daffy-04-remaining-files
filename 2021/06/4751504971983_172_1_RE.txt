KELLY TRENT

747 W Northbend Road   Cincinnati, Ohio 45224 513 473-4171
Kelly_Trent1006@yahoo.com
OBJECTIVES
To secure a position in a facility where I can continue to develop my skills and knowledge in the healthcare field
EDUCATION
Lincoln College of Technology
May, 2013 Medical Assisting
EXPERIENCE
  Nationwide Children’s Hospital   5112 Cedar Village Drive 45040
Phlebotomist August 14, 2013 – December 4, 2015

	Verifies patient by reading patient identification.
	Verifies test requisitions by comparing information with nursing station log.
	 Preparing, ordering and processing specimens.
	Obtains blood specimens by performing venipunctures, finger sticks and heal sticks.
	Tracks collected specimens by initialing, dating, and noting times of collection; maintaining daily tallies of collections performed.
	Updates job knowledge by participating in educational opportunities; reading professional publications; maintaining personal networks; participating in professional organizations.
	Serves and protects the hospital community by adhering to professional standards, hospital policies and procedures, federal, state, and local requirements, and jcaho standards.

  Modern Psychiatry & Wellness   6942 Tylersville Road 45069
Medical Assistant (Extern March 2013) April 9th 2013 ─ September 2013
	Our mission was to provide holistic, effective, compassionate treatment to those who suffer from mental illness, emotional pain, and addiction from alcohol and other drugs of abuse.
	Perform administrative duties such as greeting patients, answering phones, filing, faxing, data entry, scheduling appointments, and patient registration
	Conduct vital signs, urinalysis, and other clinical procedures
	Chart patient health information and call pharmacies for prescriptions

SKILL
	BLS certified, CMA certification
	Demonstrates strong customer service skills and professionalism
	Detail-oriented with excellent organization and communication skills
	Clinical skills include: vital signs, EKGs, minor surgery, urinalysis, injections, and phlebotomy
	Proficient in venipuncture including butterflies, syringes, vacutainers, and specimen processing
	Understanding of HIPAA, ICD-9, Medisoft, and general medical administration
	Clerical skills such as faxing, filing, data entry, and operating multiple task
	Trained in Medical Terminology, Pharmacology, and Anatomy and Physiology
	Ability to type 45+  Words Per Minute
	Great working with Children
