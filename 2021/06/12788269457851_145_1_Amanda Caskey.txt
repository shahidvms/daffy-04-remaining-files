Amanda Caskey
Peachtree Corners, Georgia, United States

amandacaskey07@yahoo.com

linkedin.com/in/amanda-caskey-17aa5b8b

Summary
Successful 5+ year track record working in a cGMP and IS0-17025 compliant laboratory. Strong organizational,
project management, multi-tasking, planning and prioritization skills. Talent for quickly mastering technology.
Great verbal and written communication skills. Diplomatic and tactful with professionals and non-professionals at
all levels. Accustomed to handling sensitive, confidential records. Demonstrated history of producing accurate,
timely reports complying with all applicable SOP, ISO procedures and/or cGMP guidelines.

Experience
Laboratory Manager
Bureau Veritas Group
Jan 2021 - May 2021 (5 months)
Managed the day to day operations of an Oil Conditioning Monitoring Lab. Managed a a team of 9
sample processing clerks and 14 lab technicians. Modified work environment to increase safety for all
personnel. Recognized on International Women's day by multiple staff members as making a difference
in their lives.

Laboratory Project Set-Up Manager
Q? Solutions
Oct 2014 - May 2020 (5 years 8 months)
Manage the process of designing and launching global or regional clinical research study databases.
Lead activities of the Sponsor/Clinical Research Organization. Guide the Project Manager and Set-up
Coordinators in designing and organizing project components.

Documentation Specialist II
Kimberly-Clark
Mar 2013 - Mar 2014 (1 year 1 month)
Write technical, validation and verification reports by analyzing the protocol and data.
Locate and collect information from various electronic systems.
Create new documents such as reports or specifications from existing information.
Transfer data, such as bills of materials, from one information system to another.
Facilitate document approvals.

ASSISTANT LAB MANAGER
Micromeritics Instrument Corporation
Feb 2011 - Oct 2012 (1 year 9 months)
Develop, optimize and/or manage method development/validation projects. Ensure projects are
carried to completion in a timely manner and performed to customers satisfaction in compliance with
SOPs/regulations.
Provide communication support to customers for each project.
Amanda Caskey - page 1
Coordinate technical help with subject matter experts on non-standard projects.
Oversee outsourced projects by communicating with contract laboratories to ensure that 1) proper
paperwork is completed/obtained, 2) projects completed within specified budget and time-frame to
customers satisfaction, and 3) analysis results meet customer specifications.
Acts on behalf of Business Manager when unavailable.
Oversee all invoicing activities for all projects. This includes making sure the system is kept up to date;
interfacing with Accounts Receivable in Accounting resolving past due collection issues with customers;
ensure that customers are being invoiced appropriately and in timely manner as well as generate credit
memos as necessary.
Provide quotes to customers.
Interface with Service Department for instrument repairs, PMs, calibrations and/or installations.
Coordinate with Training Department to ensure all Lab Personnel are up-to-date with instrument
training/certifications.
Coordinate with Lab Personnel to ensure that all areas of the lab are well-covered in the absence of
an employee.
Conduct staff meetings.
Recommend process improvements and method improvements. If approved, implement and/or
manage the implementation of approved process/method improvement.
Work closely with Lab Coordinator in the maintenance and management of sample submission
database (LIMS).
Resolve any customer related issues and unanticipated problems in a timely, efficient and cost
effective manner.
Work closely with the Compliance Specialist to ensure that all quality documentation is assembled and
maintained for audits and/or certifications.

LABORATORY ANALYST II
Micromeritics Instrument Corporation
Nov 2008 - Feb 2011 (2 years 4 months)
Able to perform all functions of Lab Analyst I
Analyze an assortment of sample types using a variety of techniques
Cross-trained in multiple areas of lab testing. Certifications for ASAP 2420, Tristar, Gemini, ASAP
2020 Chemisorption/Physisorption, Autopore, Saturn and Sedigraph.
Regular interaction with customers answering questions, explaining data, providing technical
assistance for unusual applications and other calls as received.
Assist in developing new markets and new customers.
Ability to correctly choose reports and analysis conditions without assistance.

LABORATORY ANALYST I
Micromeritics Instrument Corporation
Apr 2007 - Nov 2008 (1 year 8 months)
Analyze routine and non-routine samples using pre-defined protocols or test methods.
Contribute to the interpretation and problem solving aspects of analysis data.
Coordinate with Applications Lab/Senior Scientists to supply technical assistance when needed.
Maintain lab notebooks, logbooks, forms and other records/documentation.
Organize data results and prepare final reports/summaries. Provide Certificate of Analysis when
required.
Analyze monthly qualification standards to ensure instruments are in specification.
Maintain on-time backlog; report any late projects to supervisor and/or customers.
Amanda Caskey - page 2
Interface with customers on a limited basis to answer questions regarding results.
Assist in developing methods for new applications and/or analysis.
Actively review analytical date to ensure consistent, timely delivery of high quality and accurate
information.
Cross-train in other areas of the lab. Complete and maintain all training required.
Perform all responsibilities in accordance with company guidelines, SOPs, and industry rules and
regulations.

Education
Georgia Southern University
Bachelor of Science (B.S.), Justice Studies

Georgia Southern University
Bachelor of Science (B.S.), Chemistry

Georgia Southern University
Bachelor of Science (B.S.), Biology

Skills
Process Improvement Training Analysis Testing Software Documentation Data Analysis
Chemistry

Amanda Caskey - page 3

-----END OF RESUME-----

Resume Title: Laboratory Manager

Name: Amanda Caskey
Email: amandacaskey07@yahoo.com
Phone: true
Location: Suwanee-GA-United States-30024

Work History

Job Title: Laboratory Manager 01/01/2021 - 05/31/2021
Company Name: Bureau Veritas Group

Job Title: Laboratory Project Set-Up Manager 10/01/2014 - 05/31/2020
Company Name: Q? Solutions

Job Title: Documentation Specialist II 03/01/2013 - 03/31/2014
Company Name: Kimberly-Clark

Job Title: ASSISTANT LAB MANAGER 02/01/2011 - 10/31/2012
Company Name: Micromeritics Instrument Corporation

Job Title: LABORATORY ANALYST II 11/01/2008 - 02/01/2011
Company Name: Micromeritics Instrument Corporation

Job Title: LABORATORY ANALYST 04/01/2007 - 11/01/2008
Company Name: Micromeritics Instrument Corporation

Education

School: Georgia Southern University, Major:
Degree: Bachelor's Degree, Graduation Date:

School: Georgia Southern University, Major:
Degree: Bachelor's Degree, Graduation Date:

School: Georgia Southern University, Major:
Degree: Bachelor's Degree, Graduation Date:

Additional Skills And Qualifications

Most Recent Job Title: true
Most Recent Pay: USD per
Managed Others: true
Languages Spoken:
Work Authorization: Not Specified
Security Clearance: No
Felony Conviction: No

Desired Pay: USD per
Desired Job Types: ["Full Time","Part Time","Intern","Seasonal","Temporary","Contractor"]
Travel Preference:

Currently Employed: No
Jobs Last Three Years: 0
Last Job Tenure Months: 0
Highest Degree: 4 Year Degree
Certifications:
Employment Type:
Military Experience:
Last Updated: 5/6/2021 11:56:01 AM

Desired Shift Preferences:

Interests:

Downloaded Resume from CareerBuilder
RDF0B86HZ3VJDX6F2TS


