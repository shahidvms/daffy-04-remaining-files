Iesha Johnson

Durham, NC
ieshajohnson39@gmail.com
9195649493

Authorized to work in the US for any employer

Work Experience

Unarmed Security Officer
Admiral Security Services, Inc - Durham, NC
August 2020 to Present

I am responsible for verifying employees. Escorting contractors. Monitoring the area I am assigned to. I am
trained in badge operations which is responsible for putting employees information in the security system
as well as running different reports using Microsoft excel to organize. Sending emails for a variation of
approvals.

Detention Officer
Durham County Government - Durham, NC
August 2013 to June 2020

Make sure detainees are safe by following policies and procedures set by the company
And ensuing other employees maintained the same level of attention to detail

Cashier
McDonald's - Durham, NC
August 2012 to September 2013

Ensured clients were serviced in a timely manner
Location was clean and presentable
Assisted with additional needs as assigned by store management

Little Caesars Crew Member
Little Caesars - Durham, NC
August 2008 to August 2012

Prepped and cooked food according to company standards
Worked additional shifts to ensure work was complete as per management standards

Education

General
Hillside High School - Durham, NC
August 2007 to June 2009

Skills

* Word

* Organizational Skills

* Customer Service

* Stocking

* Security

* Cash Handling
* Typing

* Microsoft Office (6 years)

* CPR

* Cashier

* Computer Skills

* training

* Law Enforcement

* Quality Assurance

* Time Management

-----END OF RESUME-----

Resume Title: Customer Service Representative (Office and Administrative Support)

Name: Iesha Johnson
Email: ieshajohnson39@gmail.com
Phone: true
Location: Durham-NC-United States-27701

Work History

Job Title: Unarmed Security Officer 08/01/2020 - Present
Company Name: Admiral Security Services, Inc

Job Title: Detention Officer 08/01/2013 - 06/30/2020
Company Name: Durham County Government

Job Title: Cashier 08/01/2012 - 09/30/2013
Company Name: McDonald's

Job Title: Little Caesars Crew Member 08/01/2008 - 08/01/2012
Company Name: Little Caesars

Education

School: Hillside High School, Major:
Degree: High School, Graduation Date: 6/2009

Additional Skills And Qualifications

Most Recent Job Title: true
Most Recent Pay: USD per
Managed Others: true
Languages Spoken:
Work Authorization: true
Security Clearance: No
Felony Conviction: No

Desired Pay: USD per
Desired Job Types:
Travel Preference:

Currently Employed: No
Jobs Last Three Years: 0
Last Job Tenure Months: 0
Highest Degree: High School Degree
Certifications:
Employment Type:
Military Experience:
Last Updated: 6/3/2021 10:06:39 AM

Desired Shift Preferences:

Interests:
Police - Security - Defence
Guard-Security services
Security
Military

Downloaded Resume from CareerBuilder
R311Y9730L810SF60KC


